﻿var css = document.querySelector("h3");
var color1 = document.querySelector(".color1");
var color2 = document.querySelector(".color2");
var body = document.getElementById("gradient");
var boton = document.getElementById("random");

body.onload = function() {
    //funcion para agregar colores actuales en texto
    css.textContent =  "linear-gradient(to right, " + color1.value + ", " + color2.value + ")";
};


function cambioColor() {
	body.style.background = "linear-gradient(to right, " + color1.value + ", " + color2.value + ")";
	css.textContent = body.style.background + ";"
}

//se generan valores al azar para color
function colorRandom () {
	var char = '0123456789ABCDEF';
	var initial = '#';
	//se inicia con # y se escogerá 6 caracteres para el css de color en rgb.
	//dentro de Math.floor, se usa Math.random para generar un numero decimal al azar multiplicado por 16
	//se usa Math.floor para redondear el valor al numero entero mas cercano
	for (var i = 0; i < 6; i++) {
	initial += char[Math.floor(Math.random() * 16)];
	}
	//se devuelve el valor HEX para el color
	return initial;
	console.log(initial);
}

//se asignan valor 'initial' a ambos valores de color al azar
function valorRandom() {

	color1.value = colorRandom();
	color2.value = colorRandom();

	cambioColor();
}





color1.addEventListener("input", cambioColor);

color2.addEventListener("input", cambioColor);

boton.addEventListener("click", valorRandom);
